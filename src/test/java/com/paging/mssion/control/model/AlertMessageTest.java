package com.paging.mssion.control.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.paging.mission.control.model.AlertMessage;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class AlertMessageTest {
	AlertMessage testAlert;
	
	@Test
	public void TestSeverity() {
		try {
			testAlert = new AlertMessage(1001, "BATT", "20180101 23:03:05.009");
			
			assertEquals("RED LOW", testAlert.getSeverity());
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void TestSeverityUnknowValue() {
		try {
			testAlert = new AlertMessage(1001, "BATTT", "20180101 23:03:05.009");
			
			assertEquals(null, testAlert.getSeverity());
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
	}

}
