package com.paging.mission.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paging.mission.control.controller.SatelliteMonitor;

/**
 * Monitor Satellite Data. 
 * TODO, should add more test, I don't have time tonight. 
 * @author vchen
 *
 */
public class PagingMissionControl {
	
	public static void main (String args[]) {
		final Logger logger = LogManager.getLogger(PagingMissionControl.class);
		//Read file, check if the data hit the limit 
		//Issue alarm in gson
		SatelliteMonitor monitor= new SatelliteMonitor();
		
		//Read file and process the data
		monitor.readFile("src/main/resources/testdata.txt");
	}
}
