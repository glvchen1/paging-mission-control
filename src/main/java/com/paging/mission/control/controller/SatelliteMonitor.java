package com.paging.mission.control.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paging.mission.control.model.AlertMessage;
import com.paging.mission.control.model.SatelliteTelemetryData;

/**
 * Monitor Satellite input data and issue alert message if needed
 * @author vchen
 *
 */
public class SatelliteMonitor {
	final Logger logger = LogManager.getLogger(SatelliteMonitor.class);
	
	ArrayList<SatelliteTelemetryData> redLowBattList = new ArrayList<SatelliteTelemetryData>();
	ArrayList<SatelliteTelemetryData> readHighTstatList = new ArrayList<SatelliteTelemetryData>();
	ArrayList<AlertMessage> alertList = new ArrayList<AlertMessage>();
		
	
	/**
	 * Read Data File
	 * For each record, check if the data violate the rule.
	 * @param filePath
	 * 
	 */
	public void readFile (String filePath) {
		try {
			Scanner sc = new Scanner(new File(filePath));

			while(sc.hasNextLine()) {
				SatelliteTelemetryData rawData= new SatelliteTelemetryData(sc.nextLine());
				
				//check if hit limit, add to arraylist
				checkViolation(rawData);
			}
		}catch (FileNotFoundException ex) {
			logger.error("Exception: " +ex.getMessage());
		}
	}
	
	/**
	 * Check if the data violate the rule, if yes. add the data to the right data list
	 * @param rawData
	 */
	private void checkViolation(SatelliteTelemetryData rawData) {
		switch (rawData.getComponentName()) {
		case "TSTAT":
			if (rawData.getRawValue() > rawData.getRedHighLimit()) {
				//Add record to readHighTstatList
				issueAlert(readHighTstatList, rawData);	
				readHighTstatList.add(rawData);		
			}
		case "BATT":
			if (rawData.getRawValue() < rawData.getRedLowLimit()) {
				//add record to redLowBattList
				issueAlert(redLowBattList,rawData);
				redLowBattList.add(rawData);
			}	
		default:
			logger.info("Component Name not supported: " + rawData.getComponentName());
		}
	}
	
	
	/**
	 * Check if the same satelite ID has 
	 * @param dataList
	 */
	private void issueAlert(ArrayList<SatelliteTelemetryData> dataList, SatelliteTelemetryData rawData) {	
		ListIterator<SatelliteTelemetryData> listIter = dataList.listIterator(dataList.size());
			int count = 0;
			int timeDiff = 5 * 60 *1000;
			long timeInRaw = convertTimeStampToLong(rawData.getTimestamp());
		    
			while(listIter.hasPrevious()) {
				//System.out.println("In issueAlert"+ rawData.getSatelliteId());
				SatelliteTelemetryData currentData = (SatelliteTelemetryData) listIter.previous();
				long timeInAlert = convertTimeStampToLong(currentData.getTimestamp());
				
				if ((rawData.getSatelliteId() == currentData.getSatelliteId()) && ((timeInRaw - timeInAlert) < timeDiff)) {
					count ++;
				}
				
				if (count == 2) {
					//issue alert message
					AlertMessage alert= new AlertMessage(rawData.getSatelliteId(), rawData.getComponentName(), rawData.getTimestamp());
					sendAlerts(alert);
				}
			}
	}
	
	/**
	 * print alert message
	 * @param alert
	 */
	private void sendAlerts(AlertMessage alert) {
		 ObjectMapper mapper = new ObjectMapper();
		 try {
	            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alert);
	            System.out.println(jsonString);
	        } catch (JsonProcessingException ex) {
	        	logger.error("JsonProcessingException: " +ex.getMessage());
	        }
	}
	
	/**
	 * convert timestamp to long
	 * @param timestamp
	 * @return
	 */
	private long convertTimeStampToLong(String timestamp) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
			Date date = sdf.parse(timestamp);
			
			return date.getTime();
		}catch (ParseException ex) {
			logger.error("ParseException: " +ex.getMessage());
		}
		
		return 0;
	}
}
