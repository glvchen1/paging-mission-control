package com.paging.mission.control.model;

/**
 * Data structure for Alert Message
 * @author vchen
 *
 */
public class AlertMessage {
	private int satelliteId;
	private String severity;
	private String component;
	private String timestamp;
	
	public AlertMessage(int satelliteId, String component, String timestamp) {
		this.satelliteId = satelliteId;
		this.component = component;
		this.timestamp = timestamp;
		this.severity = Severity.getSeverity(component);
	}
	
	public int getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
