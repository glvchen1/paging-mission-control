package com.paging.mission.control.model;

/**
 * Date Structure for Satellite Raw Data
 * @author vchen
 *
 */
public class SatelliteTelemetryData {
	private String timestamp;
	private int satelliteId;
	private double redHighLimit;
	private double yellowHighLimit;
	private double yellowLowLimit;
	private double redLowLimit;
	private double rawValue;
	private String componentName;
	
	public SatelliteTelemetryData(String rawData) {
		//parse raw data and assign to each field of SatelliteTelemetryData
		String[] rawDataArray = rawData.split("\\|");
		
		timestamp = rawDataArray[0];
		satelliteId = Integer.parseInt(rawDataArray[1]);
		redHighLimit= Double.parseDouble(rawDataArray[2]);
		yellowHighLimit = Double.parseDouble(rawDataArray[3]);
		yellowLowLimit= Double.parseDouble(rawDataArray[4]);
		redLowLimit= Double.parseDouble(rawDataArray[5]);
		rawValue= Double.parseDouble(rawDataArray[6]);
		componentName = rawDataArray[7];
	}
	
	
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public int getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}
	
	public double getRedHighLimit() {
		return redHighLimit;
	}



	public void setRedHighLimit(double redHighLimit) {
		this.redHighLimit = redHighLimit;
	}



	public double getYellowHighLimit() {
		return yellowHighLimit;
	}



	public void setYellowHighLimit(double yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}



	public double getYellowLowLimit() {
		return yellowLowLimit;
	}



	public void setYellowLowLimit(double yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}



	public double getRedLowLimit() {
		return redLowLimit;
	}



	public void setRedLowLimit(double redLowLimit) {
		this.redLowLimit = redLowLimit;
	}



	public double getRawValue() {
		return rawValue;
	}



	public void setRawValue(double rawValue) {
		this.rawValue = rawValue;
	}



	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
}
