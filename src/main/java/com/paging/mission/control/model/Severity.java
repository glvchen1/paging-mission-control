package com.paging.mission.control.model;

/**
 * 	Convert component to severity that will be used in Alert message
 * @author vchen
 *
 */
public class Severity {
	public static String getSeverity(String component) {
		switch (component) 
		{
		case "TSTAT":
			return "RED HIGH";
		case "BATT":
			return "RED LOW";
		default:
			return null;				
		}
	}
}
